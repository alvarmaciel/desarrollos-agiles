import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Para quienes',
    Svg: require('../../static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        Este curso está orientado a personas que no tienen experiencia en desarrollo del software,
        pero quieren incorporar metodologías y herramientas de gestión propias de este
        campo para sus desarrollos profesionales.
      </>
    ),
  },
  {
    title: 'Cómo es',
    Svg: require('../../static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        Consta de encuentros sincrónicos semanales, virtuales, en formato taller de 90 minutos. Una serie de actividades entre encuentros y un espacio de consulta asincrónico.
      </>
    ),
  },
  {
    title: 'De que trata',
    Svg: require('../../static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        <ul>
          <li>Metodologías ágiles para la gestión de proyectos</li>
          <li>Git como herramienta de producción de documentos personales y colaborativos</li>
          <li>Plataformas: <a href="https://gitlab.com/" target="_blank">GitLab</a> o <a href="https://github.com/" target="_blank">Github</a></li>
      </ul>
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
