---
sidebar_position: 2
title: Estructura Funcional
---

> Este artículo está escrito en base a la lectura y notas tomadas de [Scrum: El arte de hacer el doble de trabajo en la mitad de tiempo](https://www.amazon.com/Scrum-trabajo-tiempo-definici%C3%B3n-Spanish-ebook/dp/B01EIQAALK) y el sito [Scrum Guides](https://scrumguides.org/)


![](/slides/scrum-dot.png)

Scrum se sustenta en la relación de sus valores, roles dentro del equipo, los eventos y sus artefactos.

## Descripción Funcional

![](/img/agileCicle.png)

El marco *requiere* que una persona que asuma el rol de /Scrum Master/ dentro del equipo que fomente un entorno donde:

1. Un *Product Owner* ordena el trabajo de un problema complejo en un *Product Backlog*.
2. El *Scrum Team* convierte una selección del trabajo en un *Increment* de valor durante un *Sprint*.
3. El *Scrum Team* y sus interesados inspeccionan los resultados y se adaptan para el próximo *Sprint*.
4. Repita

## Bases y Principios
- Transparencia: 
  > El proceso y el trabajo emergentes deben ser visibles tanto para quienes realizan el trabajo como para quienes lo reciben.
Las decisiones importantes se basan en el estado percibido de sus tres artefactos
La transparencia permite la inspección.
- Inspección:
  > Los artefactos de Scrum y el progreso hacia los objetivos acordados deben inspeccionarse con
  > frecuencia y con diligencia para detectar variaciones o problemas potencialmente indeseables.

- Adaptación:
> Si algún aspecto de un proceso se desvía fuera de los límites aceptables o si el producto resultante es inaceptable, el proceso que se aplica o los materiales que se producen deben ajustarse.

## Valores

- compromiso a lograr sus objetivos y a apoyarse mutuamente.
- Foco en el trabajo del Sprint para lograr el mejor progreso
- El Equipo es Franco sobre el trabajo y los desafíos.
- Se respetan entre sí para ser personas capaces e independientes
- Tienen el coraje de hacer lo correcto, para trabajar en

## Equipo y roles

- Developers (Desarrolladores del proyecto):
> La gente que está comprometida para crear cualquier aspecto de un incrementable usable en el sprint

  - Crean un plan para el Sprint. El Sprint Backlog (un artefacto)
  - Definen que se considera una tarea HECHA
  - Adaptan el plan diariamente para conseguir el objetivo del Sprint

- Product Owner (Responsable de Producto):
>Responsable de maximizar el valore del producto resultante a partir del trabajo del Scrum Team

- Tareas.
  - Desarrollar y comunicar explícitamente el objetivo del producto;
  - Crear y comunicar claramente los items del Product Backlog
  - Organizar los items
  - Asegurar la visibilidad y entendimiento.
  
- Scrum Master (Responsable del marco de trabajo)
> Es quien se encarga que mantener el Scrum como está definido, ayudando a todos a entender la práctica y la teoría.
> Sirve al Scrum Team al Scrum Owner y a la organización en general.
- Tareas: 
