---
sidebar_position: 1
title: Sintaxis de Markdown
---

# Qué es Markdown

![](https://upload.wikimedia.org/wikipedia/commons/4/48/Markdown-mark.svg)

**Markdown** es un lenguaje de marcado ligero, *sencillo*, que se usa para agregar elementos de formato a textos planos. Estos formatos luego se aplican a la visualización, en general, de páginas o contenido web.

Por ejemplo, este texto, con su título, imagen, negritas e itálicas está escrito de la forma en que se muestra debajo, y es transformado en la página que estás viendo donde se aplican los formatos.


```markdown
# Qué es Markdown

![](https://upload.wikimedia.org/wikipedia/commons/4/48/Markdown-mark.svg)

**Markdown** es un lenguaje de marcado ligero, *sencillo*, que se usa para agregar elementos de formato a textos planos. Estos formatos luego se aplican a la visualización, en general, de páginas o contenido web.

Por ejemplo, este texto, con su título, imagen, negritas e itálicas está escrito de la forma en que se muestra debajo, y es transformado en la página que estás viendo donde se aplican los formatos.

```


A continuación se detalla los elementos maś comunes de su sintaxis.

Para una lista más detallada de la sintaxis pueden chequear algunos de estos sitios
- [GitHub Markdown](https://docs.github.com/es/github/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)
- [Markdown cheatsheet](https://markdown.es/sintaxis-markdown/)

## Encabezados
Para crear un encabezado, agrega de uno a seis símbolos `#` antes del encabezado del texto. La cantidad de `#` que usas determinará el nivel del ecanbezado.

```markdown
# El encabezado de nivel 1
## El encabezado de nivel 2
### El encabezado de nivel 3
#### El encabezado de nivel 4
##### El encabezado de nivel 5
###### El encabezado de nivel 6
```

# El encabezado de nivel 1
## El encabezado de nivel 2
### El encabezado de nivel 3
#### El encabezado de nivel 4
##### El encabezado de nivel 5
###### El encabezado de nivel 6


## Estilo de texto
Puedes indicar énfasis con texto en negrita, cursiva o tachado.

| Estilo | Markdown |Ejemplo |Resultado|
|--|--|--|--|
| Negrita | `** ** o __ __`  |` **Este texto está en negrita** ` | __Este texto está en negrita__ |
| Cursiva | `* * o _ _`  | `*Este texto está en cursiva*` | _Este texto está en cursiva_ |
| Tachado | `~~ ~~` |  `~~Este texto está equivocado~~` | ~~Este texto está equivocado~~|
| Cursiva en negrita y anidada | `** ** y _ _` |  `**Este texto es _extremadamente_ importante**` | **Este texto es** _extremadamente_ **importante** |
| Todo en negrita y cursiva	| `*** ***` |  `***Todo este texto es importante***` | ***Todo este texto es importante***|


## Links
Para crear un enlace, incluya el texto del enlace entre corchetes (por ejemplo, [Duck Duck Go]) y luego sígalo inmediatamente con la URL entre paréntesis (por ejemplo, (https://duckduckgo.com)). 

```
[Duck Duck Go](https://duckduckgo.com)
```

[Duck Duck Go](https://duckduckgo.com)

## Imágenes

Para agregar una imagen, agregue un signo de exclamación (!), Seguido de texto alternativo entre paréntesis y la ruta o URL al activo de la imagen entre paréntesis. Opcionalmente, puede agregar un título después de la URL entre paréntesis. 

```
![foto en la población](https://pixelfed.social/storage/m/_v2/72770235375161344/83a90637a-595111/xrNuGhoCWHZ5/cRzu79DigtOsEkzNsuL8lUnWfLOsFrU9ZFM67Jbl.jpg)
```
![foto en la población](https://pixelfed.social/storage/m/_v2/72770235375161344/83a90637a-595111/xrNuGhoCWHZ5/cRzu79DigtOsEkzNsuL8lUnWfLOsFrU9ZFM67Jbl.jpg)


## Citas de texto
Puedes citar texto con un `>`.

```markdown
>¡Lento es normal!
>
> -[Joey Hess](https://joeyh.name/)
```

>¡Lento es normal!
>
> -[Joey Hess](https://joeyh.name/)

## Listas

Se pueden organizar items en listas ordenadas o desordenadas

### Listas ordenadas

Pueden numerarse consecutivamente

```
1. Primer item
2. Segundo item
3. Tercer item
4. Cuarto item 
```

1. Primer item
2. Segundo item
3. Tercer item
4. Cuarto item 

o no numerase

```
1. Primer item
1. Segundo item
1. Tercer item
1. Cuarto item 
```

1. Primer item
2. Segundo item
3. Tercer item
4. Cuarto item 

También pueden anidarse

```
1. Primer item
2. Segundo item
3. Tercer item
    1. item anidado
    2. item anidado
4. Cuarto item 
```


### Listas desordenadas

Para crear listas desordenadas se usan guiones `-`, numerales `*` o signos de suma `+`

```
- Primer item
- Segundo item
- Tercer item
- Cuarto item 
```

- Primer item
- Segundo item
- Tercer item
- Cuarto item 

```
* Primer item
* Segundo item
* Tercer item
* Cuarto item 
```
* Primer item
* Segundo item
* Tercer item
* Cuarto item 

```
+ Primer item
+ Segundo item
+ Tercer item
+ Cuarto item 
```

+ Primer item
+ Segundo item
+ Tercer item
+ Cuarto item 

También pueden anidarse

```
- Primer item
- Segundo item
- Tercer item
    - item anidado
    - item anidado
- Cuarto item 
```
- Primer item
- Segundo item
- Tercer item
    - item anidado
    - item anidado
- Cuarto item 

## Código
Puedes indicar un código o un comando dentro de un enunciado con comillas simples. El texto dentro de las comillas simples no será formateado.
```
Usa `git status` para enumerar todos los archivos nuevos o modificados que aún no han sido confirmados.
```
Usa `git status` para enumerar todos los archivos nuevos o modificados que aún no han sido confirmados.
Para formatear código o texto en su propio bloque distintivo, usa comillas triples.
```
Algunos de los comandos de Git básicos son:
```
git status
git add
git commit
```
```

Algunos de los comandos de Git básicos son:

```
git status
git add
git commit
```
