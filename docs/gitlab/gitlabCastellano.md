---
sidebar_position: 2
title: Pasar GitLab a Castellano 💬
---

Pasos para pasar [GitLab](https:gitlab.com "web de gitlab") a castellano

1. Ir al menú de usuario
   ![Menú de usaurio](/img/gitlabNavBar-1.png "Menú de Usuario")
2. Ir a `Preferences`.
   
   ![preferences](/img/gitlabMenuUsuarioPreferences.png "preferences")
3. Bajar hasta la sección `Localization` y cambiar el idioma a Castellano. 👀 Atención, solo está traducido el 41% del sitio 👀.

   ![Gif con localizacion](/img/gitlabLocalizacion.gif "Cambiar a castellano")

